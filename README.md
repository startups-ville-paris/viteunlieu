# ViteUnLieu

"Vite un lieu" aide les associations parisiennes à trouver rapidement un lieu pour leurs réunions, événements, activités collectives.
**public**
  The directory available for the web server. Contains subdirectories for images, stylesheets,
  and javascripts. Also contains the dispatchers and the default HTML files. This should be
  set as the DOCUMENT_ROOT of your web server.

**spec**
  Unit and functional tests along with fixtures. When using the script/generate scripts, template
  test files will be generated for you and placed in this directory.

**vendor**
  External libraries that the application depends on. Also includes the plugins subdirectory.
  If the app has frozen rails, those gems also go here, under vendor/rails/.
  This directory is in the load path.
  
## Installing

**1. Clone the app**

Browse to where you want the app to live and clone app:

```
git clone https://gitlab.com/startups-ville-paris/viteunlieu.git
```

Change directory into the viteunlieu folder
```
cd viteunlieu
```

**2. Start the app**

First install all the required gems:
```
bundle install
```

Set up and migrate the database:
```
rake db:setup
rake db:migrate
```

Lets run the App:
```
rails server
```

Then browse to [http://localhost:3000](http://localhost:3000) to view the app. 

**3. Log in as Admin**

Navigate to a protected resource and enter the following credentials (only for dev environment)

admin / admin

[http://localhost:3000/demandes](http://localhost:3000/demandes)


## Deployment

**1. Deploy to Heroku**

Following [these steps](https://devcenter.heroku.com/articles/getting-started-with-rails5)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

