$('.ui.form.demande').form({
    fields: {
      demande_nom_association : 'empty',
      demande_nom_demandeur : 'empty',
      demande_tel_demandeur : 'empty',
      demande_email_demandeur : 'email',
      demande_date : 'empty',
      demande_duree : 'empty',
      demande_nb_participants : 'decimal',
      demande_activite : 'empty'
    }
  })
;
$.datepicker.setDefaults($.datepicker.regional["fr"]);
$('.ui.form.demande #demande_date').datepicker({
});
$('#demandes-menu .item').tab();
