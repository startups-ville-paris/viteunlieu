$('.ui.form.salle').form({
    fields: {
      salle_nom : 'empty',
      salle_adresse : 'empty',
      salle_tel : 'empty',
      salle_email : 'email',
      salle_capacite : 'number',
      salle_tarif_minimum : 'decimal'
    }
  })
;
