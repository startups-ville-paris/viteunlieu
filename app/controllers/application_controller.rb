class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def admin?
    ActionController::HttpAuthentication::Basic.has_basic_credentials?(request)
  end
end
