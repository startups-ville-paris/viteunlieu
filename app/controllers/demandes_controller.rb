class DemandesController < ApplicationController
  http_basic_authenticate_with name: ENV['ADMIN_USER'], password: ENV['ADMIN_PASSWORD'], only: [:index]

  before_action :standby, only: [:new, :create]
  before_action :set_demande, only: [:show, :cloture, :notifie, :autres_salles_correspondantes]

  def standby
    if ENV["STANDBY"] == "true"
      redirect_to root_path
    end
  end

  def new
    @demande = Demande.new
  end

  def create
    @demande = Demande.new(demande_params)

    if @demande.save
      DemandeMailer.confirm(@demande).deliver
    else
      render :new
    end
  end

  def show
    if admin?
      matcher = MatcherSalles.new(@demande)
      matcher.cherche!
      @salles_correspondantes = matcher.salles
      @autres_salles_correspondantes = matcher.autres_salles_correspondantes?
      @limite = 2 * MatcherSalles::LIMITE_PROPOSITION
      render :show_admin
    else
      render :show_demandeur
    end
  end

  def index
    @demandes_en_cours = Demande.en_cours
    @demandes_cloturees = Demande.cloturees
  end

  def cloture
    @demande.cloture!
    if admin?
      redirect_to demandes_path
    else
      render 'cloturee'
    end
  end

  def notifie
    salle = Salle.find(params[:salle_id])
    NotifierSalle.new(@demande, salle).call!
    redirect_to @demande
  end

  def autres_salles_correspondantes
    if admin?
      if params[:limite]
        limite = params[:limite].to_i
        matcher = MatcherSalles.new(@demande, limite)
      else
        limite = 0
        matcher = MatcherSalles.new(@demande)
      end
      matcher.cherche!
      @salles_correspondantes = matcher.salles
      @autres_salles_correspondantes = matcher.autres_salles_correspondantes?
      @limite = limite + MatcherSalles::LIMITE_PROPOSITION
      render :show_admin
    else
      redirect_to root_path
    end
  end

  private
    def set_demande
      @demande = Demande.find_by_secret_id(params[:secret_id])
    end

    def demande_params
      params.require(:demande).permit(:nom_demandeur, :tel_demandeur, :email_demandeur, :nom_association, :date, :duree, :nb_participants, :objet, :activite, :quartier)
    end
end
