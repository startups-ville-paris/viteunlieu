class PagesController < ApplicationController
  layout 'landing', only: :home
  def home
  end

  def stats
    @nb_demandes = Demande.count
  end
end
