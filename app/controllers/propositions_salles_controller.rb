class PropositionsSallesController < ApplicationController
  http_basic_authenticate_with name: ENV['ADMIN_USER'], password: ENV['ADMIN_PASSWORD'], only: :index
  before_action :standby

  def standby
    if ENV["STANDBY"] == "true"
      redirect_to root_path
    end
  end

  def new
    @salle = Salle.new
  end

  def create
    @salle = ProposerSalle.new(salle_params).call!
    if @salle.valid?
      render :create
    else
      render :new
    end
  end

  def index
    @salles = Salle.non_confirmees.page params[:page]
    render 'salles/index'
  end

  private
    def salle_params
      params.require(:salle).
        permit(:nom_lieu, :nom_salle,
               :adresse, :code_postal, :ville, :email, :tel, :horaires_ouverture,
               :superficie, :capacite, :capacite_assemblee, :capacite_avec_tables, :capacite_cocktail,
               :tarif_minimum, :activites, :precisions)
    end
end
