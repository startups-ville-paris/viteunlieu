class SallesController < ApplicationController
  http_basic_authenticate_with name: ENV['ADMIN_USER'], password: ENV['ADMIN_PASSWORD']
  before_action :set_salle, only: [:show, :edit, :update, :destroy, :contact]

  def index
    liste Salle.confirmees
  end

  def brut
    liste Salle.brut
  end

  def prospects
    liste Salle.prospects
  end

  def prequalifiees
    liste Salle.prequalifiees
  end

  def show
  end

  def new
    @salle = Salle.new
  end

  def edit
  end

  def create
    if params[:commit] == t('salles.creation_et_ajout_submit')
      cree_et_ajoute
    else
      cree
    end
  end

  def cree
    @salle = ProposerSalle.new(salle_params).call!
    if @salle.valid?
        redirect_to salles_path, notice: "La salle #{@salle.nom} vient d'être créé"
    else
      render :new
    end
  end

  def cree_et_ajoute
    salle_modele = ProposerSalle.new(salle_params).call!
    if salle_modele.valid?
      @salle = Salle.new(nom_lieu: salle_modele.nom_lieu, email: salle_modele.email, tel: salle_modele.tel, adresse: salle_modele.adresse)
    else
      @salle = salle_modele
    end
    render :new
  end

  def contact
    contact = ContacterDemandeur.new(@salle, params[:demande_id])
    if contact.call!
      @demande = contact.demande
    else
      render 'contact_demande_cloturee'
    end
  end

  def update
    if params[:commit] == t('salles.validation_submit')
      params[:salle][:qualification] = "qualifiee"
    end
    if @salle.update(salle_params)
        redirect_to salles_path, notice: 'La salle a été modifiée avec succès'
    else
      render :edit
    end
  end

  def destroy
    @salle.destroy
    redirect_to salles_url, notice: 'La salle a été correctement supprimée !'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_salle
      @salle = Salle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def salle_params
      params.require(:salle).
        permit(:qualification, :nom_lieu, :nom_salle,
               :adresse, :code_postal, :ville, :email, :tel, :horaires_ouverture,
               :superficie, :capacite, :capacite_assemblee, :capacite_avec_tables, :capacite_cocktail,
               :tarif_heure, :tarif_demi_journee, :tarif_journee, :temps_min_reservation,
               :activites, :precisions)
    end

    def liste(salles)
      @salles = ChercherSalles.new(params, salles).call
      @nb_salles_trouvees = @salles.length
      respond_to do |format|
        format.csv do
          ExporterSalles.new("export.csv", @salles).call!
          render file: "export.csv"
        end
        format.html do
          @salles = @salles.page params[:page]
          render :index
        end
      end
    end
end
