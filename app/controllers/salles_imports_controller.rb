class SallesImportsController < ApplicationController
  http_basic_authenticate_with name: ENV['ADMIN_USER'], password: ENV['ADMIN_PASSWORD']

  def new

  end

  def create
    csv_import = params[:fichier_import]
    if csv_import == nil
      redirect_to new_salles_import_path, alert: "Aucun fichier fourni !" and return
    end

    result = ImporterSalles.new(csv_import.path).call!
    if result[:statut] == :succes
      redirect_to new_salles_import_path, notice: "#{result[:nb_salles_ajoutees]} salles ont été ajoutées" and return
    end

    if result[:statut] == :warning
      nb_salles_invalides = result[:nb_salles_invalides]
      redirect_to new_salles_import_path, alert: "#{nb_salles_invalides} salles sont invalides et n'ont pas pu être ajoutées. #{result[:nb_salles_ajoutees]} salles ont été ajoutées"
    end
  end
end
