module ApplicationHelper
  def admin?
    ActionController::HttpAuthentication::Basic.has_basic_credentials?(request)
  end

  def item_link_to(text, path)
    content_tag(:div, class: item_css_classes(path)) do
      link_to text, path
    end
  end

  def item_css_classes(path)
    css_classes = [ 'item' ]
    css_classes << 'active' if current_page?(path)
    css_classes.join(' ')
  end

  def css_salle_item(salle, demande)
    mapping = { :a_pris_contact => 'teal', :notifiee => 'purple', :a_traiter => '' }
    [ 'salle', mapping[salle.statut(demande)] ].join(' ')
  end

  def action_ou_evenements_pour_salle(salle, demande)
    evenements = salle.evenements(demande)
    if evenements.empty?
      link_to "Notifier", notifie_demande_path(@demande.secret_id, salle.id), method: :post, class: "ui button #{css_salle_item(salle, demande)}"
    else
      html_evenements = []
      evenements.map do |evenement|
        content_tag(:div, evenement_message(evenement), class: "#{evenement[:type].to_s} ui ribbon label #{css_salle_item(salle, demande)}")
      end.reduce(&:+)
    end
  end

  def evenement_message(evenement)
    case evenement[:type]
    when :notifie
      "Notifiée le #{l(evenement[:quand], format: :short)}"
    when :a_pris_contact
      "A pris contact le #{l(evenement[:quand], format: :short)}"
    end
  end
end
