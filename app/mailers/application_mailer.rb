class ApplicationMailer < ActionMailer::Base
  default from: ENV['EMAIL_NOREPLY']
  layout 'mailer'
end
