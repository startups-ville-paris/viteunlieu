class DemandeMailer < ApplicationMailer
  def confirm(demande)
    @demande = demande
    mail(to: @demande.email_demandeur, cc: ENV['EMAIL_ADMIN'], subject: "[Vite, un lieu !] Demande de salle déposée")
  end

  def notifie(demande, salle)
    @demande = demande
    @salle = salle
    mail(to: salle.email, subject: "[Vite, un lieu !] Une association a besoin de votre lieu")
  end

  def contact(demande, salle)
    @demande = demande
    @salle = salle
    mail(to: demande.email_demandeur, subject: "[Vite, un lieu !] Une salle va bientôt vous contacter")
  end
end
