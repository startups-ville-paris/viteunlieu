class Demande < ApplicationRecord
  before_create :generate_secret_id, :calcule_coordonnees!
  validates :nom_demandeur, :tel_demandeur, :email_demandeur, :nom_association,
    :date, :duree, :nb_participants, :activite, presence: true

  has_many :notifications
  has_many :contacts
  scope :en_cours, -> { order(created_at: :desc).where(cloture_le: nil) }
  scope :cloturees, -> { order(created_at: :desc).where.not(cloture_le: nil) }
  def to_param
    self.secret_id
  end

  def calcule_coordonnees!
    coordonnees = GeocoderAdresse.new("#{self.quartier}, Paris").call
    if coordonnees[:latitude].present? && coordonnees[:longitude].present?
      self.latitude = coordonnees[:latitude]
      self.longitude = coordonnees[:longitude]
    end
  end

  def statut
    return :a_traiter if a_traiter?
    return :a_contacter if a_contacter?
    return :contactee if contactee?
  end

  def contactee?
    self.contacts_count > 0
  end

  def a_contacter?
    self.contacts_count == 0
  end

  def a_traiter?
    self.notifications_count == 0
  end

  def cloturee?
    self.cloture_le.present?
  end

  def cloture!
    self.cloture_le = DateTime.now
    self.save!
  end

  def geocoded?
    self.latitude.present? && self.longitude.present?
  end

  private
  def generate_secret_id
    self.secret_id = Digest::SHA1.hexdigest "#{email_demandeur},#{Time.now.to_f}"
  end
end
