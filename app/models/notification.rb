class Notification < ApplicationRecord
  belongs_to :demande, counter_cache: true
  belongs_to :salle
  validates :salle_id, uniqueness: { scope: :demande_id }
end
