class Salle < ApplicationRecord
  validates :nom_salle, uniqueness: { scope: :nom_lieu }
  validates :nom_lieu, :adresse, :ville, :code_postal, presence: true
  has_many :notifications
  has_many :contacts

  before_save :affecte_capacite!
  before_save :calcule_coordonnees!, if: ->(salle){ salle.adresse_changed? }

  scope :confirmees, -> { where(qualification: :qualifiee) }
  scope :non_confirmees, -> { where.not(qualification: :qualifiee) }
  scope :brut, -> { where(qualification: :brut) }
  scope :prospects, -> { where(qualification: :prospect) }
  scope :prequalifiees, -> { where(qualification: :prequalifiee) }

  geocoded_by :adresse_complete

  AVEC_TABLES = :avec_tables
  ASSEMBLEE = :assemblee

  QUALIFICATIONS = [:brut, :prospect, :prequalifiee]

  def affecte_capacite!
    capacite_assemblee = self.capacite_assemblee || 0
    capacite_avec_tables = self.capacite_avec_tables || 0
    capacite_cocktail = self.capacite_cocktail || 0
    max = [capacite_assemblee, capacite_avec_tables, capacite_cocktail].max || 0
    self.capacite = max if self.capacite.nil? || max > self.capacite
  end

  def calcule_coordonnees!
    salles_meme_adresse_geocodee = Salle.where(adresse: self.adresse, code_postal: self.code_postal, ville: self.ville).where('latitude is not null')
    if salles_meme_adresse_geocodee.present?
      salle = salles_meme_adresse_geocodee.first
      self.latitude, self.longitude = salle.latitude, salle.longitude
    else
      coordonnees = GeocoderAdresse.new(self.adresse_complete).call
      if coordonnees[:latitude].present? && coordonnees[:longitude].present?
        self.latitude = coordonnees[:latitude]
        self.longitude = coordonnees[:longitude]
      end
    end
  end

  def nom
    [ self.nom_lieu, self.nom_salle ].join(' - ')
  end

  def adresse_complete
    code_postal_et_ville = [ self.code_postal, self.ville ].join(' ')
    [self.adresse, code_postal_et_ville].join(', ')
  end

  def draft?
    QUALIFICATIONS.include? self.qualification.to_sym
  end

  def notifie?(demande)
    notifications_demande(demande).present?
  end

  def notifications_demande(demande)
    self.notifications.where(demande: demande)
  end

  def contacts_demande(demande)
    self.contacts.where(demande: demande)
  end

  def statut(demande)
    return :a_pris_contact if contacts_demande(demande).present?
    return :notifiee if notifications_demande(demande).present?
    :a_traiter
  end

  def evenements(demande)
    evenements = []
    notifications_demande(demande).each do |notif|
      evenements << { type: :notifie, quand: notif.created_at }
    end

    contacts_demande(demande).each do |contact|
      evenements << { type: :a_pris_contact, quand: contact.created_at }
    end

    evenements
  end

  def date_derniere_notif(demande)
    notifications_demande(demande).last.created_at
  end
end
