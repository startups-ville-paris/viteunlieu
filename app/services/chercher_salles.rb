class ChercherSalles
  def initialize(params, salles = Salle.all)
    @salles = salles
    @params = params
  end

  def call
    salles_table = Salle.arel_table
    salles = @salles.where(salles_table[:nom_lieu].matches("%#{@params[:nom_lieu]}%"))
    salles = salles.where(salles_table[:code_postal].eq(@params[:code_postal])) if @params[:code_postal].present?
    salles = salles.where(salles_table[:source].matches("%#{@params[:source]}%")) if @params[:source].present?
    salles = salles.where(salles_table[:capacite].gteq(@params[:capacite_min])) if @params[:capacite_min].present?
    salles = salles.where(salles_table[:capacite].lteq(@params[:capacite_max])) if @params[:capacite_max].present?
    salles.order(:capacite)
  end
end
