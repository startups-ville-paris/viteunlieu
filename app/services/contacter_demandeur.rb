class ContacterDemandeur
  attr_reader :salle, :demande
  def initialize(salle, demande_id)
    @salle = salle
    @demande = @salle.notifications.where(demande: demande_id).last.demande 
  end

  def call!
    return false if demande.cloturee?
    Contact.create!(salle: salle, demande: demande)
    DemandeMailer.contact(demande, salle).deliver
  end
end
