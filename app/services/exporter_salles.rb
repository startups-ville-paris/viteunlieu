require 'csv'

class ExporterSalles
  def initialize(filename, salles)
    @filename, @salles = filename, salles
  end

  def call!
    CSV.open(@filename, 'wb', col_sep: "\t", headers: true) do |csv|
      csv << headers
      @salles.each do |salle|
        csv << [
          salle.id, salle.source, salle.qualification,
          salle.nom_lieu, salle.adresse, salle.code_postal, salle.ville,
          salle.tel, salle.email, salle.web,
          salle.nom_salle, salle.superficie, salle.capacite_assemblee, salle.capacite_avec_tables, salle.capacite_cocktail,
          salle.tarif_heure, salle.tarif_demi_journee, salle.tarif_journee,
          salle.temps_min_reservation
        ]
      end
    end
  end

  def headers
    [
      "Id", "Source", "Qualification", "Nom Lieu", "Adresse", "Code Postal", "Ville", "Tel", "Email", "Web", "Nom Salle",
      "Superficie", "Nb Places Assemblée", "Nb Places Table", "Nb Places Cocktail",
      "Tarif heure", "Tarif demi-journée", "Tarif journée",
      "Temps minimum réservation (h)"
    ]
  end
end
