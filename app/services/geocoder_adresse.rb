class GeocoderAdresse
  def initialize(adresse)
    @adresse = adresse
  end

  def call
    response = JSON.parse(HTTParty.get("http://nominatim.openstreetmap.org/search?q=#{URI.encode(@adresse)}&format=json", headers: {"User-Agent" => "viteunlieu"}).body)
    if response.length > 0
      { latitude: response[0]["lat"].to_f, longitude: response[0]["lon"].to_f }
    else
      {}
    end
  end
end
