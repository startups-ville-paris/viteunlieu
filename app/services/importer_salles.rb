require 'csv'

class ImporterSalles
  def initialize(filename)
    @filename = filename
  end

  def call!
    converters = lambda {|colonne|
      nouvelle_colonne = I18n.transliterate(colonne.downcase.gsub(' ', '_'))
      nouvelle_colonne = "capacite_avec_tables" if nouvelle_colonne.include?("table")
      nouvelle_colonne = "capacite_cocktail" if nouvelle_colonne.include?("cocktail")
      nouvelle_colonne = "capacite_assemblee" if nouvelle_colonne.include?("assemblee")
      nouvelle_colonne
    }

    nb_salles_ajoutees = 0
    nb_salles_invalides = 0
    Salle.skip_callback(:save, :before, :calcule_coordonnees!)
    CSV.foreach(@filename, headers: true, col_sep: "\t", header_converters: converters) do |row|
      attributes = row.to_hash
      id_salle = attributes.delete("id")
      if id_salle.present?
        salle = Salle.find(id_salle)
        salle.update_attributes(attributes.slice(*Salle.column_names))
      else
        salle = ProposerSalle.new(row.to_hash.slice(*Salle.column_names)).call!
      end
      if salle.valid?
        nb_salles_ajoutees += 1
      else
        nb_salles_invalides += 1
      end
    end
    Salle.set_callback(:save, :before, :calcule_coordonnees!)

    {
      statut:nb_salles_invalides > 0 ? :warning : :succes,
      nb_salles_ajoutees: nb_salles_ajoutees,
      nb_salles_invalides: nb_salles_invalides
    }
  end
end
