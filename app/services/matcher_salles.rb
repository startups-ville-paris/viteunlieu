class MatcherSalles
  attr_reader :demande, :salles
  LIMITE_PROPOSITION = 7

  def initialize(demande, limite = LIMITE_PROPOSITION)
    @demande = demande
    @salles = []

    @limite = limite
    @limite = LIMITE_PROPOSITION unless limite
    @limite.to_i
  end

  def cherche!
    @salles = Salle.confirmees
    if demande.disposition.present?
      @salles = @salles.where("capacite_#{demande.disposition} >= :participants OR capacite >= :participants", participants: demande.nb_participants).order("capacite_#{demande.disposition}")
    else
      @salles = @salles.where('capacite >= ?', demande.nb_participants)
    end
    if demande.geocoded?
      @salles = @salles.geocoded.near([demande.latitude, demande.longitude], 50, order: 'capacite, distance')
    end
    @autres_salles_correspondantes = @salles.count(:all) > @limite
    @salles = @salles.limit(@limite)
    true
  end

  def autres_salles_correspondantes?
    @autres_salles_correspondantes
  end
end
