class NotifierSalle
  attr_reader :demande, :salle
  def initialize(demande, salle)
    @demande, @salle = demande, salle
  end

  def call!
    DemandeMailer.notifie(demande, salle).deliver if Notification.create(demande: demande, salle: salle)
  end
end
