class ProposerSalle
  def initialize(salle_params)
    @salle = Salle.new(salle_params)
  end

  def call!
    @salle.qualification = :brut
    @salle.save
    @salle
  end
end
