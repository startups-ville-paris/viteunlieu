if Rails.env.test?
  FakeWeb.allow_net_connect = false
  FakeWeb.register_uri(:get, %r|http://nominatim.openstreetmap.org|,
                       body: JSON.generate(["lat": "48.8684992", "lon": "2.352093"]))
end
