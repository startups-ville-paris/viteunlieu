Rails.application.routes.draw do
  post 'salles/cree_et_ajoute_dans_meme_lieu', to: 'salles#cree_et_ajoute', as: :cree_salle_et_ajoute_dans_meme_lieu
  root 'pages#home'
  get 'a-propos', to: 'pages#apropos'
  get 'stats', to: 'pages#stats'

  resources :salles do
    collection do
      get 'brut'
      get 'prospects'
      get 'prequalifiees'
    end
  end
  get 'salles/:id/contact/:demande_id', to: 'salles#contact', as: :contacte_demandeur
  resources :propositions_salles, only: [:new, :create]
  resource :salles_import, only: [:new, :create]

  resources :demandes, only: [:new, :create, :show, :index], param: :secret_id
  post 'demandes/:secret_id/cloture', to: 'demandes#cloture', as: :cloture_demande
  get 'demandes/:secret_id/autres_salles_correspondantes', to: 'demandes#autres_salles_correspondantes', as: :autres_salles_correspondantes
  post 'demandes/:secret_id/notifie/:salle_id', to: 'demandes#notifie', as: :notifie_demande
  mount LetterOpenerWeb::Engine, at: '/mails'
end
