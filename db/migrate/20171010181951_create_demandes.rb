class CreateDemandes < ActiveRecord::Migration[5.0]
  def change
    create_table :demandes do |t|
      t.string :nom_demandeur
      t.string :tel_demandeur
      t.string :email_demandeur
      t.string :nom_association
      t.string :date
      t.string :duree
      t.integer :nb_participants
      t.string :objet
      t.text :precisions

      t.timestamps
    end
  end
end
