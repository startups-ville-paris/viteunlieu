class AddSecretIdToDemandes < ActiveRecord::Migration[5.0]
  def change
    add_column :demandes, :secret_id, :string
    add_index :demandes, :secret_id
  end
end
