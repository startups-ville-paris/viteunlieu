class CreateSalles < ActiveRecord::Migration[5.0]
  def change
    create_table :salles do |t|
      t.string :nom
      t.string :adresse
      t.string :email
      t.string :tel
      t.string :secret_id
      t.text :horaires_ouverture
      t.integer :capacite
      t.integer :tarif_minimum
      t.string :activites
      t.text :precisions

      t.timestamps
    end
    add_index :salles, :secret_id
  end
end
