class AddActiviteAndRemovePrecisionsToDemandes < ActiveRecord::Migration[5.0]
  def change
    add_column :demandes, :activite, :text
    remove_column :demandes, :precisions
  end
end
