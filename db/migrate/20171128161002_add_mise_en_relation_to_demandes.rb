class AddMiseEnRelationToDemandes < ActiveRecord::Migration[5.0]
  def change
    add_column :demandes, :mise_en_relation, :datetime
  end
end
