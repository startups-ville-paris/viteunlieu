class RenommeMiseEnRelationParClotureLePourDemandes < ActiveRecord::Migration[5.0]
  def change
    rename_column :demandes, :mise_en_relation, :cloture_le
  end
end
