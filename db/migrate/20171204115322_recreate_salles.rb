class RecreateSalles < ActiveRecord::Migration[5.0]
  def change
    create_table :salles do |t|
      t.string :nom
      t.string :adresse
      t.string :email
      t.string :tel
      t.text :horaires_ouverture
      t.integer :capacite
      t.integer :tarif_minimum
      t.string :activites
      t.text :precisions

      t.timestamps
    end
  end
end
