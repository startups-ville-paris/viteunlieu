class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :demande, foreign_key: true
      t.belongs_to :salle, foreign_key: true

      t.timestamps
    end
  end
end
