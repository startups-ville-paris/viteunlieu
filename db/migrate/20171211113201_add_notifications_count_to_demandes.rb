class AddNotificationsCountToDemandes < ActiveRecord::Migration[5.0]
  def up
    add_column :demandes, :notifications_count, :integer, null:false, default: 0
    Demande.reset_column_information
    ids = Set.new
    Notification.all.each {|n| ids << n.demande_id}
    ids.each do |demande_id|
      Demande.reset_counters demande_id, :notifications
    end
  end

  def down
    remove_column :demandes, :notifications_count
  end
end
