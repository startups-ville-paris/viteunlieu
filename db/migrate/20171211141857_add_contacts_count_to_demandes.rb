class AddContactsCountToDemandes < ActiveRecord::Migration[5.0]
  def up
    add_column :demandes, :contacts_count, :integer, null:false, default: 0
    Demande.reset_column_information
    ids = Set.new
    Contact.all.each {|n| ids << n.demande_id}
    ids.each do |demande_id|
      Demande.reset_counters demande_id, :contacts
    end
  end

  def down
    remove_column :demandes, :contacts_count
  end
end
