class AddConfigToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :capacite_theatre, :integer
    add_column :salles, :capacite_avec_tables, :integer
  end
end
