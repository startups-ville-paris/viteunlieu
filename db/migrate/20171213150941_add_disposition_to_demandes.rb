class AddDispositionToDemandes < ActiveRecord::Migration[5.0]
  def change
    add_column :demandes, :disposition, :string
  end
end
