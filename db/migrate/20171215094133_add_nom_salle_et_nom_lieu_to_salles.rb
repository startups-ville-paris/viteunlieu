class AddNomSalleEtNomLieuToSalles < ActiveRecord::Migration[5.0]
  def change
    rename_column :salles, :nom, :nom_salle
    add_column :salles, :nom_lieu, :string
  end
end
