class AddDraftToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :draft, :boolean, default: false
  end
end
