class AddCodePostalAndVilleToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :code_postal, :string
    add_column :salles, :ville, :string
  end
end
