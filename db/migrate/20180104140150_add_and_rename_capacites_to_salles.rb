class AddAndRenameCapacitesToSalles < ActiveRecord::Migration[5.0]
  def change
    rename_column :salles, :capacite_theatre, :capacite_assemblee
    add_column :salles, :capacite_cocktail, :integer
    add_column :salles, :superficie, :integer
  end
end
