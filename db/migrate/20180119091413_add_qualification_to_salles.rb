class AddQualificationToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :qualification, :string, default: :brut
    remove_column :salles, :draft
  end
end
