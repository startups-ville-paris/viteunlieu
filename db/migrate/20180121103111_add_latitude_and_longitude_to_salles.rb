class AddLatitudeAndLongitudeToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :latitude, :float
    add_column :salles, :longitude, :float
  end
end
