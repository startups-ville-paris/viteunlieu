class AddTarifsToSalles < ActiveRecord::Migration[5.0]
  def change
    add_column :salles, :tarif_heure, :integer
    add_column :salles, :tarif_demi_journee, :integer
    add_column :salles, :tarif_journee, :integer
    add_column :salles, :temps_min_reservation, :integer
  end
end
