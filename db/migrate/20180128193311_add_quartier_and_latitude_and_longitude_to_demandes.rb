class AddQuartierAndLatitudeAndLongitudeToDemandes < ActiveRecord::Migration[5.0]
  def change
    add_column :demandes, :quartier, :string
    add_column :demandes, :latitude, :float
    add_column :demandes, :longitude, :float
  end
end
