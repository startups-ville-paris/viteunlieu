# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180128193311) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.integer  "demande_id"
    t.integer  "salle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["demande_id"], name: "index_contacts_on_demande_id", using: :btree
    t.index ["salle_id"], name: "index_contacts_on_salle_id", using: :btree
  end

  create_table "demandes", force: :cascade do |t|
    t.string   "nom_demandeur"
    t.string   "tel_demandeur"
    t.string   "email_demandeur"
    t.string   "nom_association"
    t.string   "date"
    t.string   "duree"
    t.integer  "nb_participants"
    t.string   "objet"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "secret_id"
    t.integer  "budget"
    t.text     "activite"
    t.datetime "cloture_le"
    t.integer  "notifications_count", default: 0, null: false
    t.integer  "contacts_count",      default: 0, null: false
    t.string   "disposition"
    t.string   "quartier"
    t.float    "latitude"
    t.float    "longitude"
    t.index ["secret_id"], name: "index_demandes_on_secret_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "demande_id"
    t.integer  "salle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["demande_id"], name: "index_notifications_on_demande_id", using: :btree
    t.index ["salle_id"], name: "index_notifications_on_salle_id", using: :btree
  end

  create_table "salles", force: :cascade do |t|
    t.string   "nom_salle"
    t.string   "adresse"
    t.string   "email"
    t.string   "tel"
    t.text     "horaires_ouverture"
    t.integer  "capacite"
    t.integer  "tarif_minimum"
    t.string   "activites"
    t.text     "precisions"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "capacite_assemblee"
    t.integer  "capacite_avec_tables"
    t.string   "nom_lieu"
    t.string   "code_postal"
    t.string   "ville"
    t.integer  "capacite_cocktail"
    t.integer  "superficie"
    t.string   "web"
    t.string   "source"
    t.string   "qualification",         default: "brut"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "tarif_heure"
    t.integer  "tarif_demi_journee"
    t.integer  "tarif_journee"
    t.integer  "temps_min_reservation"
  end

  add_foreign_key "contacts", "demandes"
  add_foreign_key "contacts", "salles"
  add_foreign_key "notifications", "demandes"
  add_foreign_key "notifications", "salles"
end
