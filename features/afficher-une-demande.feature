# language: fr
Fonctionnalité: Afficher une demande

  Scénario: afficher une demande avec plusieurs salles correspondantes
    Etant donné 1 demande pour 20 participants avec un budget de 20
    Et les salles suivantes:
      | capacite | tarif_minimum | qualification |
      | 25       | 15            | qualifiee     |
      | 20       | 20            | qualifiee     |
      | 18       | 20            | qualifiee     |
    Et que je suis administrateur
    Et que je suis sur la page "Espace administration"
    Quand je veux voir les salles correspondantes à la demande
    Alors je dois voir 2 salles dans la zone "salles.correspondantes"

