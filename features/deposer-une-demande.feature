# language: fr
Fonctionnalité: Déposer une demande de salle

  Scénario: déposer une demande valide
    Etant donné que je souhaite trouver une salle
    Quand je dépose la demande suivante
      | nom            | association | role      | tel      | email                 | date       | durée   | nb_participants | activité      |
      | Jeanne Calment | SeniorPlus  | Tresorier | 01234567 | jeanne@seniorplus.org | 28/10/2017 | 1h      | 20              | Réunion de CA |
    Alors un mail avec comme objet "[Vite, un lieu !] Demande de salle déposée" est envoyé à "jeanne@seniorplus.org" avec en copie "viteunlieu@futur.paris" par "viteunlieu@futur.paris"
    Et ce mail contient "Bonjour Jeanne Calment"
    Et ce mail contient "Vous avez déposé la demande suivante pour le compte de SeniorPlus"
    Et ce mail contient "20 participants"
    Et ce mail contient "Activité: Réunion de CA"


  Scénario: déposer une demande invalide, sans le nom de l'association
    Etant donné que je souhaite trouver une salle
    Quand je dépose la demande suivante
      | nom            |  role      | tel      | email                 | date       | durée   | nb_participants | objet         | precisions |
      | Jeanne Calment |  Tresorier | 01234567 | jeanne@seniorplus.org | 28/10/2017 | 1h      | 20              | Réunion de CA | Le wifi fonctionne bien ? |
    Alors j'obtiens le message d'erreur "Nom association est obligatoire"

  Scénario: visualiser une demande déposée
    Etant donné la demande suivante déposée
      | nom            | association | role      | tel      | email                 | date       | durée   | nb_participants | activité      |
      | Jeanne Calment | SeniorPlus  | Tresorier | 01234567 | jeanne@seniorplus.org | 28/10/2017 | 1h      | 20              | Réunion de CA |
    Quand je souhaite cloturer la demande
    Alors je dois voir "SeniorPlus"
    Et je dois voir "Réunion de CA"
