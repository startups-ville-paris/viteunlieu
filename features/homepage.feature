# language: fr
Fonctionnalité: Lire la proposition de valeur

  Scénario: depuis la page d'accueil

    Quand je visite la page d'accueil
    Alors je vois le nom du produit "Vite, un lieu !"
    Et je vois la proposition de valeur "Paris vous aide à trouver"
