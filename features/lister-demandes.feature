# language: fr
Fonctionnalité: Lister les demandes

  Scénario: afficher les demandes en cours
    Etant donné 3 demandes en cours
    Et que je suis administrateur
    Et que je suis sur la page d'accueil
    Quand je vais sur la page "Espace administration"
    Alors je dois voir 3 demandes dans la zone "demandes-en-cours"

  Scénario: afficher les demandes déjà cloturees
    Etant donné 3 demandes en cours
    Et 5 demandes cloturees
    Et que je suis administrateur
    Et que je suis sur la page d'accueil
    Quand je vais sur la page "Espace administration"
    Alors je dois voir 5 demandes dans la zone "demandes-cloturees"

  Scénario: cloturer une demande
    Etant donné 1 demandes en cours
    Et que je suis administrateur
    Et que je suis sur la page "Espace administration"
    Quand je cloture la demande en cours
    Alors la demande est cloturee
