# language: fr
Fonctionnalité: Notifier une salle

  Scénario: Notifier une salle
    Etant donné 1 demande déposée
    Et 2 salles correspondantes avec comme email "contact@lapaillasse.org"
    Et que je suis administrateur
    Et que je suis sur la page "Demande"
    Quand je notifie la première salle
    Alors un mail avec comme objet "[Vite, un lieu !] Une association a besoin de votre lieu" est envoyé à "contact@lapaillasse.org" par "viteunlieu@futur.paris"
    Et je dois voir que seule la première salle a été notifiée
