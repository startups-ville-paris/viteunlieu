# language: fr
Fonctionnalité: Indiquer le nombre total de demandes

  Scénario: afficher le nombre de total de demandes
    Etant donné 3 demandes déposées
    Et que je suis sur la page d'accueil
    Quand je vais sur la page "Statistiques"
    Alors je dois voir "3" dans la zone "nb_demandes"

