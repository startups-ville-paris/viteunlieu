Etantdonné("je suis sur la page d'accueil") do
  visit root_path
end

Quand("je vais sur la page {string}") do |page|
  click_link page
end

Quand("je vais sur la page de la demande") do
  visit demande_path(Demande.last)
end

Quand("je veux voir les salles correspondantes à la demande") do
  click_link 'Traîter'
end
Alors("je dois voir {string} dans la zone {string}") do |text, zone|
  within ".#{zone}" do
    expect(page).to have_text(text)
  end
end

Alors("je dois voir {int} demandes dans la zone {string}") do |nb_demandes, zone|
  within ".#{zone}" do
    expect(page).to have_selector(".demande", count: nb_demandes)
  end
end

Alors("je ne dois pas voir {string} dans la zone {string}") do |text, zone|
  within ".#{zone}" do
    expect(page).not_to have_text(text)
  end
end

Etantdonné("1 demande pour {int} participants avec un budget de {int}") do |nb_participants, budget|
  FactoryBot.create(:demande, nb_participants: nb_participants, budget: budget)
end

Etantdonné(/^(\d+) demandes* déposées*$/) do |nb_demandes|
  nb_demandes.times { FactoryBot.create(:demande) }
end

Etantdonné("{int} demandes en cours") do |nb_demandes|
  nb_demandes.times { FactoryBot.create(:demande, :en_cours) }
end

Etantdonné("{int} demandes cloturees") do |nb_demandes|
  nb_demandes.times { FactoryBot.create(:demande, :cloture) }
end


Etantdonné("je suis administrateur") do
  page.driver.browser.basic_authorize 'admin', 'admin'
end

Etantdonné('je suis sur la page "Espace administration"') do
  visit demandes_path
end

Etantdonné('je suis sur la page "Demande"') do
  visit demande_path(Demande.last)
end

Quand("je cloture la demande en cours") do
  within '.demandes-en-cours' do
    click_link('Cloturer')
  end
end

Alors("la demande est cloturee") do
  expect(Demande.first.cloture_le).not_to be_nil
end
