Etantdonnéque("je souhaite trouver une salle") do
  visit new_demande_path
end

Etantdonnéque("la demande suivante déposée") do |table|
  creer_demande(table.hashes.first)
end

Etantdonné("les demandes suivantes déposées") do |table|
  table.hashes.each {|demande| creer_demande(demande) }
end

Quand("je dépose la demande suivante") do |table|
  demande = table.hashes.first
  fill_in :demande_nom_association, with: demande['association']
  fill_in :demande_nom_demandeur, with: demande['nom']
  fill_in :demande_tel_demandeur, with: demande['tel']
  fill_in :demande_email_demandeur, with: demande['email']
  fill_in :demande_date, with: demande["date"]
  fill_in :demande_duree, with: demande["durée"]
  fill_in :demande_nb_participants, with: demande["nb_participants"].to_i
  fill_in :demande_activite, with: demande["activité"]
  click_button "Déposer la demande"
end

Alors(/^j'obtiens le message d'erreur "([^"]*)"$/) do |message|
  within '#explication_erreurs' do
    expect(page).to have_text(message)
  end
end

Quand("je souhaite cloturer la demande") do
  visit demande_path(Demande.last)
end

Alors("je dois voir {string}") do |text|
  expect(page).to have_text(text)
end
