Quand("je visite la page d'accueil") do
  visit root_path
end

Alors("je vois le nom du produit {string}") do |produit|
  within '#nom-produit' do
    expect(page).to have_text(produit)
  end
end

Alors("je vois la proposition de valeur {string}") do |proposition_valeur|
  within '#proposition-valeur' do
    expect(page).to have_text(proposition_valeur)
  end
end

