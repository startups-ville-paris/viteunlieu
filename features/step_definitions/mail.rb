Alors(/^un mail avec comme objet "(.*?)" est envoyé à "(.*?)" avec en copie "(.*?)" par "(.*?)"$/) do |sujet, email_demandeur, admin, expediteur|
  demande_mail = last_mail

  expect(demande_mail.from).to eq [expediteur]
  expect(demande_mail.subject).to eq(sujet)
  expect(demande_mail.cc).to eq [admin]
  expect(demande_mail.to[0]).to eq(email_demandeur)
end

Alors("un mail avec comme objet {string} est envoyé à {string} par {string}") do |sujet, email_demandeur, expediteur|
  demande_mail = last_mail

  expect(demande_mail.from).to eq [expediteur]
  expect(demande_mail.subject).to eq(sujet)
  expect(demande_mail.to[0]).to eq(email_demandeur)
end

Alors("ce mail contient {string}") do |text|
  expect(CGI::unescapeHTML(last_mail.html_part.body.to_s)).to include(text)
  expect(last_mail.text_part.body.to_s).to include(text)
end

