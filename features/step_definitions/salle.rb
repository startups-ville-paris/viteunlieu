Quand("je propose la salle suivante") do |table|
  visit new_salle_path
  salle = table.hashes.first
  fill_in :salle_nom, with: salle['nom']
  fill_in :salle_adresse, with: salle['adresse']
  fill_in :salle_email, with: salle['email']
  fill_in :salle_tel, with: salle['tel']
  fill_in :salle_horaires_ouverture, with: salle['horaires_ouverture']
  fill_in :salle_capacite, with: salle['capacite']
  fill_in :salle_tarif_minimum, with: salle['tarif_minimum']
  fill_in :salle_activites, with: salle['activités']
  fill_in :salle_precisions, with: salle['précisions']
  click_button 'Proposer la salle'
end

Etantdonné("{int} salles") do |nb_salles|
  nb_salles.times { FactoryBot.create(:salle) }
end

Etantdonné("les salles suivantes:") do |table|
  salles = table.hashes
  salles.each do |salle_args|
    FactoryBot.create(:salle, capacite: salle_args[:capacite], tarif_minimum: salle_args[:tarif_minimum], qualification: salle_args[:qualification])
  end
end

Etantdonné("{int} salles correspondantes avec comme email {string}") do |nb, email|
  nb.times { FactoryBot.create(:salle, capacite: Demande.last.nb_participants, email: email, qualification: :qualifiee)}
end

Etantdonnéque('je suis sur la page "Salles"') do
  visit salles_path
end

Etantdonnéque("je veux ajouter une salle") do
  click_link 'Ajouter une salle'
end

Quand("j'ajoute la salle {string}") do |nom_salle|
  fill_in :salle_nom_salle, with: nom_salle
  click_button "Créer la salle"
end

Alors("je dois voir la salle {string}") do |salle|
  within '.salles .nom' do
    expect(page).to have_text(salle)
  end
end

Alors("je dois voir {int} salles dans la zone {string}") do |nb_salles, zone|
  within ".#{zone}" do
    expect(page).to have_selector(".salle", count: nb_salles)
  end
end

Quand("je notifie la première salle") do
  within "#salle-#{Salle.first.id}" do
    click_link 'Notifier'
  end
end

Alors("je dois voir que seule la première salle a été notifiée") do
  within "#salle-#{Salle.first.id}" do
    expect(page).to have_selector(".notifie", count: 1)
  end
  within "#salle-#{Salle.last.id}" do
    expect(page).to have_selector(".notifie", count: 0)
  end
end
