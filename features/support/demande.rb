def creer_demande(demande)
  Demande.create(
    nom_demandeur: demande['nom'],
    tel_demandeur: demande['tel'],
    email_demandeur: demande['email'],
    nom_association: demande['association'],
    date: demande['date'],
    duree: demande['durée'],
    nb_participants: demande['nb_participants'],
    objet: demande['objet'],
    activite: demande['activité'],
    budget: demande['budget']
  )
end
