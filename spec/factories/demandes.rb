FactoryBot.define do
  factory :demande do
    nom_demandeur Faker::Name.name
    tel_demandeur Faker::PhoneNumber.phone_number
    email_demandeur Faker::Internet.email
    nom_association Faker::Company.name
    date Faker::Date.backward(30)
    duree "2h"
    nb_participants { [5, 30, 100, 500][rand(3)] }
    activite { ["Assemblée Générale", "Réunion publique"][rand(1)] }
    budget { [10,20,50, 300, 1000][rand(4)] }

    trait :en_cours do
    end

    trait :cloture do
      cloture_le { Time.now }
    end
  end
end
