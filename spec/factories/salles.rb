FactoryBot.define do
  factory :salle do
    nom_lieu Faker::Company.name
    sequence(:nom_salle) {|n| "salle-#{n}"}
    adresse Faker::Address.street_address
    code_postal Faker::Address.zip
    ville Faker::Address.city
    capacite 20
    tarif_minimum 10
    email Faker::Internet.email
    web Faker::Internet.url
  end
end
