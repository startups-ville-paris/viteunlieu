require 'rails_helper'

feature 'contacter demandeur' do
  scenario 'avec une demande non clôturée' do
    demande = FactoryBot.create(:demande)
    salle = FactoryBot.create(:salle)
    NotifierSalle.new(demande, salle).call!

    page.driver.browser.basic_authorize 'admin', 'admin'
    visit contacte_demandeur_path(salle.id, demande.id)

    expect(page).to have_content(demande.nom_demandeur)
    expect(page).to have_content(demande.tel_demandeur)
    expect(page).to have_content(demande.email_demandeur)

    last_mail = ActionMailer::Base.deliveries.last
    expect(last_mail.to[0]).to eq(demande.email_demandeur)
    expect(last_mail.subject).to eq("[Vite, un lieu !] Une salle va bientôt vous contacter")

  end

  scenario 'avec une demande clôturée' do
    demande = FactoryBot.create(:demande)
    salle = FactoryBot.create(:salle)
    NotifierSalle.new(demande, salle).call!
    demande.cloture!
    nb_mails = ActionMailer::Base.deliveries.length

    page.driver.browser.basic_authorize 'admin', 'admin'
    visit contacte_demandeur_path(salle.id, demande.id)

    expect(page).to have_content("Désolé ! Cette demande a déjà trouvé un lieu")
    expect(ActionMailer::Base.deliveries.length).to eq(nb_mails)
  end
end
