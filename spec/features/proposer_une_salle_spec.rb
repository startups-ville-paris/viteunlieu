require 'rails_helper'
require_relative 'support'

feature 'proposer une salle' do
  scenario "Un internaute propose une salle" do
    salle = FactoryBot.
      build(:salle,
                 nom_lieu: 'La Paillasse',
                 adresse: '226 rue Saint-Denis, 75002 Paris',
                 nom_salle: 'Salle voûtée')

    visit root_path
    click_on 'Proposer une salle'
    fill_in_salle_form(salle)
    click_on 'Proposer la salle'

    expect(page).to have_text('Merci de votre contribution !')
    sign_as_admin
    visit salles_path
    expect(page).not_to have_text(salle.nom_lieu)
    visit brut_salles_path
    expect(page).to have_text(salle.nom_lieu)

  end

  scenario "L'administrateur ajoute une salle brute" do
    salle = FactoryBot.
      build(:salle,
                 nom_lieu: 'La Paillasse',
                 adresse: '226 rue Saint-Denis, 75002 Paris',
                 nom_salle: 'Salle voûtée')
    sign_as_admin
    visit salles_path
    click_on 'Ajouter une salle'
    fill_in_salle_form(salle)
    click_on 'Créer la salle'

    visit salles_path
    expect(page).not_to have_text(salle.nom_lieu)
    visit brut_salles_path
    expect(page).to have_text(salle.nom_lieu)
  end

  scenario "L'administrateur ajoute une salle dans un même lieu" do
    salle = FactoryBot.
      build(:salle,
                 nom_lieu: 'La Paillasse',
                 adresse: '226 rue Saint-Denis, 75002 Paris',
                 nom_salle: 'Salle voûtée')

    sign_as_admin
    visit new_salle_path
    fill_in_salle_form(salle)

    click_on 'Créer la salle et en ajouter une autre dans ce lieu'

    expect(Salle.last.nom_salle).to eq(salle.nom_salle)
    expect(find_field('salle_nom_lieu').value).to eq(salle.nom_lieu)
    expect(find_field('salle_nom_salle').value).to be_nil
  end

  def fill_in_salle_form(salle)
    fill_in :salle_nom_lieu, with: salle.nom_lieu
    fill_in :salle_nom_salle, with: salle.nom_salle
    fill_in :salle_adresse, with: salle.adresse
    fill_in :salle_code_postal, with: salle.code_postal
    fill_in :salle_ville, with: salle.ville
    fill_in :salle_email, with: salle.email
    fill_in :salle_web, with: salle.web
    fill_in :salle_tel, with: salle['tel']
    fill_in :salle_horaires_ouverture, with: salle['horaires_ouverture']
    fill_in :salle_superficie, with: salle['superficie']
    fill_in :salle_capacite, with: salle['capacite']
    fill_in :salle_capacite_assemblee, with: salle['capacite']
    fill_in :salle_capacite_avec_tables, with: salle['capacite']
    fill_in :salle_capacite_cocktail, with: salle['capacite']
    fill_in :salle_activites, with: salle['activités']
    fill_in :salle_precisions, with: salle['précisions']
  end
end
