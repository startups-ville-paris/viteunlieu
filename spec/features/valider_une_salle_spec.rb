require 'rails_helper'

feature 'valider une salle' do
  scenario "L'administrateur valide une salle proposée" do
    salle = FactoryBot.create(:salle, qualification: :prequalifiee)
    sign_as_admin
    visit edit_salle_path(salle)

    click_on "Valider la salle"
    visit salles_path

    expect(page).to have_text(salle.nom_lieu)
    visit prequalifiees_salles_path
    expect(page).not_to have_text(salle.nom_lieu)

  end
end
