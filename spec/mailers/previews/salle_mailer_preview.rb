# Preview all emails at http://localhost:3000/rails/mailers/salle_mailer
class SalleMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/salle_mailer/creation
  def creation
    SalleMailerMailer.creation
  end

end
