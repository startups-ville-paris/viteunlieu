require 'rails_helper'

describe ChercherSalles do
  describe '#call' do
    it 'retourne la salle correpondant au nom du lieu' do
      salle_recherchee = FactoryBot.
        create(:salle,
                   nom_lieu: 'La Paillasse',
                   nom_salle: 'Salle voûtée')
      salle_non_recherchee = FactoryBot.
        create(:salle,
                   nom_lieu: 'CICP',
                   nom_salle: 'Salle 2')

      salles = ChercherSalles.new({nom_lieu: 'paillasse'}).call

      expect(salles.length).to eq 1
      expect(salles).to include(salle_recherchee)
      expect(salles).not_to include(salle_non_recherchee)
    end

    it 'retourne la salle correspondant au code postal' do
      salle_recherchee = FactoryBot.create(:salle, code_postal: '75018')
      salle_non_recherchee = FactoryBot.create(:salle, code_postal: '75020')

      salles = ChercherSalles.new({code_postal: '75018'}).call

      expect(salles.length).to eq 1
      expect(salles).to include(salle_recherchee)
      expect(salles).not_to include(salle_non_recherchee)

    end

    it 'retourne la salle correspondant à la source' do
      salle_recherchee = FactoryBot.create(:salle, source: 'mda')
      salle_non_recherchee = FactoryBot.create(:salle, source: 'office du tourisme')

      salles = ChercherSalles.new({source: 'mda'}).call

      expect(salles.length).to eq 1
      expect(salles).to include(salle_recherchee)
      expect(salles).not_to include(salle_non_recherchee)

    end
  end

end
