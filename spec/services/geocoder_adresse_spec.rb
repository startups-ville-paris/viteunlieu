require 'rails_helper'

describe GeocoderAdresse do
  describe '#call' do
    it 'retourne la latitude et la longitude' do
      adresse = "226 rue Saint Denis, Paris"
      nominatim_response = double(body: JSON.generate(["lat": "48.8684992", "lon": "2.352093"]))
      expect(HTTParty).to receive(:get).with(/#{URI.encode adresse}/, headers: {"User-Agent" => "viteunlieu"}).and_return(nominatim_response)

      coordonnees = GeocoderAdresse.new(adresse).call

      expect(coordonnees).to eq({latitude: 48.8684992, longitude: 2.352093})
    end

    it "retourne {} si l'adresse n'est pas reconnue" do
      adresse = "226 rue de Saint Denis, Paris"
      nominatim_response = double(body: JSON.generate([]))
      expect(HTTParty).to receive(:get).with(/#{URI.encode adresse}/, headers: {"User-Agent" => "viteunlieu"}).and_return(nominatim_response)

      coordonnees = GeocoderAdresse.new(adresse).call

      expect(coordonnees).to eq({})

    end
  end
end
