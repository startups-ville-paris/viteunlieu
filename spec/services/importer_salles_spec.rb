require 'rails_helper'

describe ImporterSalles do
  describe "call!" do
    it "devrait importer des salles depuis un fichier" do
      salle_voutee = ["source test", "La Paillasse", "226 rue Saint-Denis", "75002", "Paris", nil, nil, "lapaillasse.org", "Salle voûtée", "50", nil, nil]
      salle_hall = ["source test", "La Paillasse", "226 rue Saint-Denis", "75002", "Paris", nil, nil, "lapaillasse.org", "Hall", "40", nil, nil]
      genere_csv("csv_filename", [salle_voutee, salle_hall])

      result = ImporterSalles.new("csv_filename").call!

      expect(result[:statut]).to eq :succes
      expect(Salle.count).to eq 2
      expect(Salle.find_by(nom_lieu: "La Paillasse").capacite_assemblee).to eq 50
      expect(Salle.find_by(nom_lieu: "La Paillasse").source).to eq "source test"
    end

    it "devrait renvoyer les salles invalides" do
      salle = ["source test", "La Paillasse", "226 rue Saint-Denis", "75002", "Paris", nil, nil, "lapaillasse.org", "Salle voûtée", "50", nil, nil]
      salle_identique = ["source test", "La Paillasse", "226 rue Saint-Denis", "75002", "Paris", nil, nil, "lapaillasse.org", "Salle voûtée", "50", nil, nil]
      genere_csv("csv_filename", [salle, salle_identique])

      result = ImporterSalles.new("csv_filename").call!

      expect(result[:statut]).to eq :warning
      expect(Salle.count).to eq 1
      expect(result[:nb_salles_invalides]).to eq 1

    end
  end

  def genere_csv(filename, salles)
    file = CSV.generate(col_sep: "\t") do |csv|
      csv << ["Source", "Nom Lieu", "Adresse", "Code Postal", "Ville", "Tel", "Email", "Web", "Nom Salle", "Nb Places Assemblée", "Nb Places Table", "Nb Places Cocktail"]
      salles.each {|salle| csv << salle}
    end

    expect(File).to receive(:open).with("csv_filename", any_args).and_return(file)
  end
end
