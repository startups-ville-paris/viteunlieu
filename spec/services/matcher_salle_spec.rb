require 'rails_helper'

describe MatcherSalles do
  describe 'cherche!' do
    it "devrait trouver aucune salle quand aucune salle n'existe" do
      demande = FactoryBot.create(:demande)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles).to eq []
    end

    it "devrait trouver une salle si une salle correspond" do
      salle = FactoryBot.create(:salle, capacite: 30, qualification: :qualifiee)
      demande = FactoryBot.create(:demande, nb_participants: 30)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles).to eq [salle]
    end

    it "devrait trouver 2 salles triées" do
      salle_40 = FactoryBot.create(:salle, capacite: 40, qualification: :qualifiee)
      salle_30 = FactoryBot.create(:salle, capacite: 30, qualification: :qualifiee)
      demande = FactoryBot.create(:demande, nb_participants: 30)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles).to eq [salle_30, salle_40]
    end

    it "devrait trouver que les salles qui correspondent" do
      salle_10 = FactoryBot.create(:salle, capacite: 10, qualification: :qualifiee)
      salle_30 = FactoryBot.create(:salle, capacite: 30, qualification: :qualifiee)
      demande = FactoryBot.create(:demande, nb_participants: 30)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles).to eq [salle_30]
    end

    it "devrait trouver une salle en fonction de sa disposition" do
      salle_avec_tables = FactoryBot.create(:salle, capacite_avec_tables: 40, qualification: :qualifiee)
      salle_assemblee = FactoryBot.create(:salle, capacite_assemblee: 40, qualification: :qualifiee)
      demande = FactoryBot.create(:demande, nb_participants: 40, disposition: Salle::ASSEMBLEE)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles).to eq [salle_assemblee, salle_avec_tables]
    end

    it "devrait trouver une salle si la capacité convient, même si la disposition n'est pas renseignée" do
      salle_capacite = FactoryBot.create(:salle, capacite: 40, qualification: :qualifiee)
      salle_assemblee = FactoryBot.create(:salle, capacite_assemblee: 40, qualification: :qualifiee)
      demande = FactoryBot.create(:demande, nb_participants: 40, disposition: Salle::ASSEMBLEE)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.salles.sort).to eq [salle_assemblee, salle_capacite].sort
    end

    it "ne devrait trouver que 7 salles même s'il y en davantage qui matchent" do
      10.times { FactoryBot.create(:salle, capacite: 50, qualification: :qualifiee) }
      demande = FactoryBot.create(:demande, nb_participants: 40)
      matcher = MatcherSalles.new(demande)
      expect(matcher.autres_salles_correspondantes?).to be_nil
      matcher.cherche!
      expect(matcher.salles.count(:all)).to eq 7
      expect(matcher.autres_salles_correspondantes?).to be_truthy
    end

    it "ne devrait pas y avoir d'autres salles correspondantes" do
      7.times { FactoryBot.create(:salle, capacite: 50, qualification: :qualifiee) }
      demande = FactoryBot.create(:demande, nb_participants: 40)
      matcher = MatcherSalles.new(demande)
      matcher.cherche!
      expect(matcher.autres_salles_correspondantes?).to be_falsy
    end

    it "il y a une limite du nombre de salles correspondates paramétrable" do
      10.times { FactoryBot.create(:salle, capacite: 50, qualification: :qualifiee) }
      demande = FactoryBot.create(:demande, nb_participants: 40)
      matcher = MatcherSalles.new(demande, 8)
      matcher.cherche!
      expect(matcher.salles.count(:all)).to eq 8
    end
  end
end
